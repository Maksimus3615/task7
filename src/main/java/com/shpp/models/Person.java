package com.shpp.models;

import javax.persistence.*;

@Entity
@Table(name = "PERSONS")
public class Person {
    @Id
    @GeneratedValue
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "person_name")
    private String name;

    @Column(name = "person_last_name")
    private String lastName;

    @Column(name = "person_ipn")
    private String ipn;

    public Person() {
        // it is empty
    }
    public Person(PersonDTO personDTO) {
        this.name = personDTO.getName();
        this.lastName = personDTO.getLastName();
        this.ipn = personDTO.getIpn();
    }
    public Long getId() {
        return id;
    }

    public Person setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Person setName(String name) {
        this.name = name;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public Person setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public String getIpn() {
        return ipn;
    }

    public Person setIpn(String ipn) {
        this.ipn = ipn;
        return this;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", ipn='" + ipn + '\'' +
                '}';
    }
}
