package com.shpp.models;

import com.shpp.validation.IpnChecker;

@IpnChecker
public class PersonDTO {
    private String name;

    private String lastName;



    private String ipn;

    public PersonDTO(String name, String lastName, String ipn) {
        this.name = name;
        this.lastName = lastName;
        this.ipn = ipn;
    }
    public PersonDTO() {
    }

    public String getName() {
        return name;
    }

    public PersonDTO setName(String name) {
        this.name = name;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public PersonDTO setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public String getIpn() {
        return ipn;
    }

    public PersonDTO setIpn(String ipn) {
        this.ipn = ipn;
        return this;
    }
}
