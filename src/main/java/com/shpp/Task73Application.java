package com.shpp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task73Application {

    public static void main(String[] args) {

        SpringApplication.run(Task73Application.class, args);

    }
}
