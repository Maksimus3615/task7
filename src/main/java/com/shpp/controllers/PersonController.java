package com.shpp.controllers;

import com.shpp.models.PersonDTO;
import com.shpp.models.Person;
import com.shpp.repository.PersonRepo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "", produces = "application/json")
@Api(tags = "Controller API")
@Tag(name = "Controller API", description = "This controller manages database PERSONS...")
public class PersonController {
    static final Logger log = LoggerFactory.getLogger(PersonController.class);
    private final PersonRepo personRepo;

    public PersonController(PersonRepo personRepo) {
        this.personRepo = personRepo;
    }

    @PostMapping("/add")
    @ApiOperation("this method adds person in db... ")
    public ResponseEntity<Person> addPerson(@Valid PersonDTO personDTO, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
        log.info("Starts new @PostMapping...");
        Person person = new Person(personDTO);
        personRepo.save(person);
        log.info("New person is saved...");
        return ResponseEntity.status(HttpStatus.CREATED).body(person);
    }

    @PutMapping("/put")
    @ApiOperation("this method changes person by id... ")
    public ResponseEntity<Person> putPerson(Long id, @Valid PersonDTO personDTO, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
        log.info("Starts new @PutMapping...");
        Person newPerson = new Person(personDTO);
        newPerson.setId(id);
        Person person = personRepo.findPersonById(id);
        if (person == null) {
            personWasNotFound(id);
            return ResponseEntity.notFound().build();
        }
        personRepo.save(newPerson);
        log.info("New person is saved...");
        return ResponseEntity.status(HttpStatus.CREATED).body(person);
    }

    @DeleteMapping("/del")
    @ApiOperation("this method deletes person by id... ")
    public ResponseEntity<Void> delPerson(Long id) {
        log.info("Starts new @DeleteMapping...");
        Person person = personRepo.findPersonById(id);
        if (person == null){
            personWasNotFound(id);
            return ResponseEntity.notFound().build();}
        personRepo.delete(person);
        log.info("New person is deleted...");
        return ResponseEntity.ok().build();
    }

    @GetMapping("/get_all_persons")
    @ApiOperation("this method finds all persons in db... ")
    public List<Person> getAllPersons() {
        log.info("Starts new @GetMapping: get all persons...");
        return personRepo.findAll();
    }

    @GetMapping("/get_one_person")
    @ApiOperation("this method finds a person by id... ")
    public ResponseEntity<Person> getPerson(Long id) {
        log.info("Starts new @GetMapping: get one person by id...");
        Person person = personRepo.findPersonById(id);
        if (person == null){
            personWasNotFound(id);
            return ResponseEntity.notFound().build();}
        log.info("The person was found: {}", person);
        return ResponseEntity.ok(person);
    }

    private void personWasNotFound(long id){
        log.info("The person with id={} was not found...", id);
    }
}
