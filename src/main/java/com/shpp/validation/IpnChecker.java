package com.shpp.validation;

import javax.validation.Constraint;
import javax.validation.Payload;

import java.lang.annotation.*;

@Constraint(validatedBy = IpnCheckerImpl.class)
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface IpnChecker {
    String message() default "IPN is not valid";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
