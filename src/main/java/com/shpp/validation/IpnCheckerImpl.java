package com.shpp.validation;

import com.shpp.models.PersonDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;


public class IpnCheckerImpl implements
        ConstraintValidator<IpnChecker, PersonDTO> {
    static final Logger log = LoggerFactory.getLogger(IpnCheckerImpl.class);
    private static final int IPN_SIZE = 10;
    private static final int[] PARAM = {-1, 5, 7, 9, 4, 6, 10, 5, 7};

    @Override
    public boolean isValid(PersonDTO personDTO, ConstraintValidatorContext context) {
        log.info("Start creating new person...");
        String ipn = personDTO.getIpn();
        if (ipn.length() != IPN_SIZE) {
            log.info("IPN is not valid: {}", ipn);
            log.info("-->size is not equal {}...", IPN_SIZE);
            personDidNotPassValidation();
            return false;
        }

        for (int i = 0; i < ipn.length(); i++) {
            if (!Character.isDigit(ipn.charAt(i))) {
                log.info("IPN is not valid: {}", ipn);
                log.info("-->not all symbols are digits...");
                personDidNotPassValidation();
                return false;
            }
        }

        int sum = 0;
        for (int i = 0; i < ipn.length() - 1; i++) {
            sum = sum + PARAM[i] * getDigit(ipn, i);
        }
        int lastDigit = IPN_SIZE - 1;
        boolean isSumValid = getDigit(ipn, lastDigit) == (sum % 11) % 10;

        log.info("IPN is: {}", ipn);
        if (isSumValid) {
            log.info("The control sum of IPN is valid...");
            log.info("New person passed the validation...");
        } else {
            log.info("The control sum of IPN is not valid...");
            personDidNotPassValidation();
        }
        return isSumValid;
    }

    private int charToInt(char digit) {
        return digit - '0';
    }

    private int getDigit(String ipn, int i) {
        return charToInt(ipn.charAt(i));
    }

    private void personDidNotPassValidation() {
        log.info("New person did not pass the validation...");
    }
}