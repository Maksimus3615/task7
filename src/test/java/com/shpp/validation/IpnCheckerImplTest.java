package com.shpp.validation;

import com.shpp.models.PersonDTO;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.validation.ConstraintValidatorContext;

import static org.junit.jupiter.api.Assertions.*;

class IpnCheckerImplTest {

    @Test
    void isValid() {
        PersonDTO person = new PersonDTO("Max", "Otto", "2938009211");
        IpnCheckerImpl checker = new IpnCheckerImpl();
        ConstraintValidatorContext context = Mockito.mock(ConstraintValidatorContext.class);
        assertTrue(checker.isValid(person, context));
    }

    @Test
    void isNotValidBecauseLengthIsNotValid() {
        PersonDTO person = new PersonDTO("Max", "Otto", "2938");
        IpnCheckerImpl checker = new IpnCheckerImpl();
        ConstraintValidatorContext context = Mockito.mock(ConstraintValidatorContext.class);
        assertFalse(checker.isValid(person, context));
    }

    @Test
    void isNotValidBecauseNotAllSymbolsAreDigit() {
        PersonDTO person = new PersonDTO("Max", "Otto", "123456789q");
        IpnCheckerImpl checker = new IpnCheckerImpl();
        ConstraintValidatorContext context = Mockito.mock(ConstraintValidatorContext.class);
        assertFalse(checker.isValid(person, context));
    }

    @Test
    void isNotValidBecauseControlSumIdIsNotValid() {
        PersonDTO person = new PersonDTO("Max", "Otto", "1234567898");
        IpnCheckerImpl checker = new IpnCheckerImpl();
        ConstraintValidatorContext context = Mockito.mock(ConstraintValidatorContext.class);
        assertFalse(checker.isValid(person, context));
    }
}